// ./src/services/eventHub.js
import Vue from 'vue'

const vm = new Vue()

export const $on = (name, cb) => {
  vm.$on(name, cb)
}

export const $emit = (name, payload) => {
  vm.$emit(name, payload)
}
