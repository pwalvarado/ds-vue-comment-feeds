// ./src/services/localStorage.js
export const set = (name, data) => {
    window.localStorage.setItem(name, JSON.stringify(data))
};

export const get = (name) => {
    const item = window.localStorage.getItem(name)
    if (!item) {
        window.localStorage.setItem(name, JSON.stringify({}))
    }
    return JSON.parse(item)
};

export const remove = (name) => {
    window.localStorage.removeItem(name);
};
